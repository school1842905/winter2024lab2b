import java.util.Scanner;

public class GameLauncher {
	public static void main(String args[]) {
			Scanner reader = new Scanner(System.in);
			System.out.println(" -- Hi, please write 1 for WORDLE or 2 for HANGMAN -- ");
			
			int choice = reader.nextInt();
			reader.nextLine(); // here for safety 
			
			if (choice == 1) {
					String word = Wordle.generateWord();
					Wordle.runGame(word);
	
			}
			else if (choice == 2) {
		
					// WARNING: IT CAN ONLY ACCEPT A LENGTH OF FOUR OR ELSE THERE IS A RUNTIME ERROR
					System.out.println("Enter your word:"); 
					String word = reader.next();
					// convert word to uppercase
				    String uppercase = "";
					for (int i = 0; i < word.length(); i++) {
			
					uppercase += WordGuesser.toUpperCase(word.charAt(i));
					}
					word = uppercase;
	
					WordGuesser.runGame(word);
					
			}
			else {
					System.out.println("BAD INPUT");
			}
		
				
		
	}
}