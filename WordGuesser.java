 import java.util.Scanner;
 
  public class WordGuesser {
	

	public static char toUpperCase(char c) {
		c = Character.toUpperCase(c);
		return c;
	}
	
	//Method runGame
	public static void runGame(String wordGuessed){
	Scanner reader = new Scanner(System.in);
	  
	  
	 /* boolean letterZero = false;
	 boolean letterOne = false;
	 boolean letterTwo = false;
	 boolean letterThree = false;*/
	
	// create array to store all guessed letters as booleans
	 boolean[] guessed = new boolean[]{false, false, false, false}; 

	
	  int fails = 0;
	  
	  // while loop runs while letters remain unguessed and less than 6 mistakes have been made
	  while (fails < 6 && !(guessed[0] && guessed[1] && guessed[2] && guessed[3])) {
	    System.out.println("Enter your letter:");
	  
	    char letter = reader.next().charAt(0);
	    int position = isLetterInWord(wordGuessed, letter);
	  
		// check to see if the position is valid
		if (position >= 0 && position < 4) {
			// have boolean at that position become equal to true
			guessed[position] = true;
			
		}
		else {
			fails++;
		}
		
	    System.out.println();
	    printWord(wordGuessed, guessed);
	  }
	}


	//Method isLetterInWord
	public static int isLetterInWord (String word, char character){
	  int letterPosition = 0;
		while (letterPosition < word.length()){
		  if (character == word.charAt(letterPosition)) {
			return letterPosition;
		  }
			letterPosition++;
		}
			return -1;

	}

	
	//Method printWord
	public static void printWord (String word, boolean[] guessed) {
		
	  String resultDisplay = "Your result is ";
		
		
		for (int i = 0; i < word.length(); i++) {
			// check if letter was guessed
			if (guessed[i]) {
				// add letter to result display
				resultDisplay += word.charAt(i);
			}
			// if unguessed, "_" is printed instead
			else {
				resultDisplay += " _ ";
			}
	
		}
		
	
	  System.out.println(resultDisplay);
		
	}
  }