import java.util.Random;
import java.util.Scanner;

public class Wordle {
	public static void main(String[] args) {
		String word = generateWord();
		runGame(word);
		
		
		
		
	}
		
	// generates the word to be guessed	
	public static String generateWord() {
		Random randGen = new Random();
		// create array of 20 words
		String[] words = new String[]{"HELPS", "WRITE", "PLAYS", "WORKS", "RUNES", "JUMPS", "WORDS", "FALSE", "WIPES", "WHITE", "BLACK", "BROWN", "TRUCK", "CRAZY", "BRAVE", "PRIZE", "BREAK", "DRIVE", "LIGHT", "PRIME", "LUCKY"};
		
		// get random number and then use it as the index of words to get a random word
		int num = randGen.nextInt(21);
		String word = words[num];
		
		return word;
	}
	
	
	// checks if the letter is in the word
	public static boolean letterInWord(String word, char letter) {
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == letter) {
				//System.out.println("true");
				return true;
			}
			else {
				
			}
		}
		//System.out.println("false");
		return false;
	}
	
	// checks if the letter is in the correct position in the word
	public static boolean letterInSlot(String word, char letter, int position) {
		if (word.charAt(position) == letter) {
			//System.out.println("true");
			return true;
		}
		//System.out.println("false");
		return false;
	}
	
	// returns an array of colours representing whether guessed
	public static String[] guessWord(String answer, String guess) {
			String[] colours = new String[5];
			
			// check for the colour for every letter
			for (int i = 0; i < answer.length(); i++) {
				char letter = guess.charAt(i); 
				
				// find if letter is in slot or in word
				boolean inSlot = letterInSlot(answer, letter, i);
				boolean inWord = letterInWord(answer, letter);
				
				
				// use if statements to assign a color based on if these booleans are true or false
				if (inSlot == true) {
					colours[i] = "green";
				}
				else if (inWord == true) {
					colours[i] = "yellow";
				}
				else { // inSlot and inWord are false
					colours[i] = "grey";
				}	
			}
			
			return colours;
	}
	

	// present the guessed word in the right colors
	public static void presentResults(String word, String[] colours) { // idk if this works yet
		for (int i = 0; i < word.length(); i++) {
			// print based on color
			if (colours[i] == "green") {
				// if it's the last letter, we want to switch to white after so that the following text doesn't appear in green
				if (i == 4) {
				System.out.println("\u001B[32m" + word.charAt(i) + "\u001B[0m");
				}
				else {
				System.out.print("\u001B[32m" + word.charAt(i));
				}
			}
			else if (colours[i] == "yellow") {
				if (i == 4) {
				System.out.println("\u001B[33m" + word.charAt(i) + "\u001B[0m");
				}
				else {
				System.out.print("\u001B[33m" + word.charAt(i));
				}
			}
			else if (colours[i] == "grey") {
				if (i == 4) {
				System.out.println("\u001B[0m" + word.charAt(i) + "\u001B[0m");
				}
				else {
				System.out.print("\u001B[0m" + word.charAt(i));
				}
			}
			// this was used for testing purposes, but is still useful so i know that i have an issue with my colours array
			else {
				System.out.println("Something went wrong");
			}
		}
	}
	
	// takes the guess from user and capitalizes it
	public static String readGuess() { 
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Guess the word:");
		String guess = reader.nextLine();
		
		// while the length isn't 5, keep on asking for a different guess
		while (guess.length() != 5) {
			System.out.println("The word is 5 letters");
			guess = reader.nextLine();
		}
		
		// capitalizes the guess
		guess = guess.toUpperCase();
		
		return guess;
		
	}	
		
	// runs the game using the previous methods
	public static void runGame(String word) {
		
		final int MAX_ATTEMPTS = 6;
		String guess = "AAAAA"; // must be initialized outside of the loop or the if statement after wont work
		
		// runs game 6 times using readGuess(), guessWord(), and presentResults()
		for (int i = 0; i < MAX_ATTEMPTS; i++) {
			guess = readGuess();
			String[] colours = guessWord(word, guess);
			presentResults(guess, colours);
			
			// if guess is equal to word, user wins and break from loop
			if (guess.equals(word)) {
				System.out.println("You win!");
				break;
			}
		}
			
		// loop is over, if guess still doesnt equal word, user loses 
		if (guess.equals(word) == false) {
			System.out.println("You lost. The word was " + word +". Try again.");
		}
	}
}